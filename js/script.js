"use strict";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  
  get name() {
    return this._name;
  }


  set name(newName) {
    this._name = newName;
  }

  
  get age() {
    return this._age;
  }

  
  set age(newAge) {
    this._age = newAge;
  }


  get salary() {
    return this._salary;
  }


  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  
  get lang() {
    return this._lang;
  }

  
  set lang(newLang) {
    this._lang = newLang;
  }


  get salary() {
    return super.salary * 3;
  }
}

// Створення екземплярів об'єкта Programmer
const programmer1 = new Programmer("John Doe", 25, 60000, ["Java", "C++"]);
const programmer2 = new Programmer("Alice Smith", 28, 70000, ["JavaScript", "Python"]);
const programmer3 = new Programmer("Bob Johnson", 30, 80000, ["C#", "Ruby"]);

// Виведення об'єктів у консоль
console.log("Programmer 1:", programmer1);
console.log("\nProgrammer 2:", programmer2);
console.log("\nProgrammer 3:", programmer3);


